package br.com.itau.eatingpizza.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EatingPizza {
    @JsonProperty(value = "nome_cliente_pizza")
    private String user;
    @JsonProperty(value = "local_comendo_pizza")
    private Address address;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
