package br.com.itau.eatingpizza.service;

import br.com.itau.eatingpizza.client.AddressClient;
import br.com.itau.eatingpizza.dto.Address;
import br.com.itau.eatingpizza.dto.EatingPizza;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EatingService {

    @Autowired
    AddressClient addressClient;

    private Address getAddress(String cep) {
        Address address = new Address();
        address = addressClient.findByCep(cep);

        return address;
    }

    public EatingPizza getEatingPizza(String user, String cep) {
        EatingPizza eatingPizza = new EatingPizza();
        eatingPizza.setAddress(getAddress(cep));
        eatingPizza.setUser(user.substring(0, 1).toUpperCase() + user.substring(1));
        return eatingPizza;
    }

}
