package br.com.itau.eatingpizza.client;

import br.com.itau.eatingpizza.dto.Address;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "address")
public interface AddressClient {
    @GetMapping("/{cep}")
    Address findByCep(@PathVariable(value = "cep") String cep);
}
