package br.com.itau.eatingpizza.controller;

import br.com.itau.eatingpizza.dto.Address;
import br.com.itau.eatingpizza.dto.EatingPizza;
import br.com.itau.eatingpizza.service.EatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EatingController {
    @Autowired
    private EatingService eatingService;

    @GetMapping("/{user}/{cep}")
    public EatingPizza whereIsEstingPizza(@PathVariable(value = "user") String user, @PathVariable(value = "cep") String cep) {
        EatingPizza eatingPizza = eatingService.getEatingPizza(user, cep);
        return eatingPizza;
    }
}
